import {locales} from './config'
// import {PageSection} from "../src/consts";

export type Locale = (typeof locales)[number]

// export type Strings = {
//     [key in Locale]: {
//         [key: string]: string | object
//     }
// }

// export type MkMenuItem = {
//     href: string,
//     as: string,
//     archorText: string,
//     subMenu?: MkSubMenuItem[],
//     clkCallBack?: Function | null
// }

// export type MkSubMenuItem = {
//     name: string | null,
//     menuItems: MkMenuItem[],
//     href?: string,
//     as?: string
// }

// export type MifDevelop = {
//
//     href: string,
//     as: string,
//     archorText: ArchorText,
//     imgSrc: string,
//     ribbonStr: string,
//
// }

// export type ArchorText = {
// [PageSection.courseName]: string,
// [PageSection.courseType]: string,
// }

// course header form
export interface CFPlaceHolder {
    firstName: string
    lastName: string
    email: string
    phone: string
    message: string
}

export interface CFTopIcons {
    city: string
    // [PageSection.courseType]: string,
    applyNow: string
}

export interface CFRequiredField {
    hasName: boolean // default is true
    hasCompany: boolean
    hasPhone: boolean // default true
    hasMessage: boolean // default false
    applyConstantBtn: string
}

export interface CFHeader {
    h1: string
    h2: string
    topIcons?: CFTopIcons
}

export interface ContactForm {
    formHeader?: CFHeader
    placeHolder: CFPlaceHolder
    requiredField: CFRequiredField
}

export enum HeaderType {
    Color = `color`,
    Image = `image`,
    Form = `form`,
    Video = `video`,
    LocalVideo = `localVideo`,
    HTML = `html`,
    Null = ``
}

export enum PageIndex {
    dataanalyst = 'dataanalyst', // data analyst
    datascientist = 'datascientist', // data scientist
    datasengineer = 'dataengineer', // data engineer
    daPythonSql = 'daPythonSql', // python & SQL coding实战
    daProject = 'daProject', // 企业级核心项目实战
    daTableau = 'daTableau',
    daSas = 'daSas',
    daMl = 'daMl',
    daBaomu = 'daBaomu',

    uiux = 'uiux',
    fsd = 'fsd', // full stack developer
    front = 'front', // front end
    web = 'web', // web core base
    devops = 'devops',
    ba = 'ba', // business data analyst
    qa = 'qa', // QA
    bcapp = 'bcapp', // bootcamp application
    home = 'home', // home page
    careerService = 'careerService', // Hardskill-Boost Service page
    bootcamp = 'bootcamp',
    techHub = 'techHub',
    collaboration = 'collaboration',
    itSolutions = 'itSolutions',
    recruit = 'recruit',
    techSupport = 'techSupport',
    course = 'course', // course index page
    about = 'about',
    becomeInstructor = 'becomeInstructor'
}

export enum CourseType {
    fullTime = 'Full/Part Time',
    partTime = 'Part Time'
}

export enum CourseCategory {
    it = 'IT',
    data = 'Data',
    jobSearching = 'jobSearching'
}

export interface LeafCardType {
    title: string
    content: string
}

export interface LeadCardSection {
    h1: string
    h2: string
    border?: string
    shadow?: string
    titleColor?: string
    textColor?: string
    data: LeafCardType[]
}

export interface Tuition {
    slug: PageIndex
    deposit: number
    total: number
    success: number
}

export interface FooterSection {
    subscribeUs: string
    emailPlaceHolder: string
    subscribe: string
    aboutUs: string
    contactUs: string
}

export interface TuitionInfo {
    h1: string
    h2: string
    depTag: string
    // [PageSection.courseType]: string,
    // [PageSection.courseName]: string,
    tuition: Tuition
}

export interface SideTopEnrollment {
    type: HeaderType
    content: JSX.Element | string | ContactForm
}

export interface TopEnrollmentType {
    bg: {
        type: HeaderType // color, image
        value: string
    }
    right: SideTopEnrollment
    left: SideTopEnrollment
    slogan: string
}

export enum SYSLANG {
    en = 'en',
    zh = 'zh',
    tw = 'tw'
}

export interface SectionCard {
    h1?: string
    h2?: string
    cards: any[]

    [index: string]: any
}

export interface PageMeta {
    title: string
    content: string
    keywords: string
}

export interface OverviewType {
    title: string
    content: string
    overview: string
    platinum: string
    diamond: string
}

export interface advData {
    icon: string
    title: string
    content: string
}

export interface platinum {
    data: [
        {
            title: string
            content: string
            subCard: [string]
        }
    ]
    notice: {
        title: string
        content: [string]
    }
}

export interface diamond {
    data: [
        {
            title: string
            content: string
            subCard: [string]
        }
    ]
    notice: {
        title: string
        content: [string]
    }
}

export function isLocale(tested: string): tested is Locale {
    return locales.some((locale: any, index: number) => locale === tested)
}
