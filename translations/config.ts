export const defaultLocale = `zh` as const

export const locales = [`en`, `zh`, `tw`] as const

export const languageNames = {
    en: `EN`,
    tw: `繁體`,
    // fr: `Français`,
    // pl: `Polski`,
    zh: `中文`
}
