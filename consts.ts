import moment from "moment/moment";
import {SYSLANG} from "./types";

export default {
    allLang: [SYSLANG.zh, SYSLANG.en, SYSLANG.tw],
    defaultLang: SYSLANG.zh
}
export const cnxs_ = 'winbigbass.com'
export const CNX_ = 'Win Big Bass Group'
export const BASIC_INFORMATION = {
    // BASEURL: 'https://api-lms.mark2win.com/_cid_/kfftwyf74vmA/mailer',
    mainEmail: `xianwei.lu@mail.utoronto.ca`,
    senderName: `${CNX_}`,
    emailSubject: `${CNX_} Thank you for your application`,
    website: `https://${cnxs_}`
}

export const baseURL = BASIC_INFORMATION.website
export const POSTS_PER_PAGE = 8

export const CNXCN_ = '赢取大鲈鱼联盟'
export const OFWEB_ADDRESS = {
    city: '加拿大',
    name: CNX_,
    nameZh: CNXCN_,
    address: [`CA: 13A-20 St Moritz Way, Markham L3R 4G4`],
    postal: ``,
    email: `xianwei.lu@mail.utoronto.ca`,
    phone: `+1 647870-7315`,
    flatPhone: ``
}

export const OFWEB_DATE = {
    foundedDate: 2013,
    yearsOfService: moment().year() - 2013 + 1,
    consultant: 9,
    successCasse: 8000,
    employer: 2200,
    customer: '3万'
}

export const SEOMapHTML = {
    title: 'title',
    metaDesc: 'description'
}


