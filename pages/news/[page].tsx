import React from 'react'
import withLocaleStaticPage from '../../hocs/withLocaleStatic'

import consts, {baseURL, POSTS_PER_PAGE} from '../../consts'
import NewsArticleCard from '../../components/Helper/News/NewsArticleCard'
import {
    getAllNewsArticleSlugs,
    getAllNewsArticles,
    getArticlePostsForNews,
    getPostImage,
    getPostVideoUrl,
    getVideoPostsForNews
} from '../../src/graphqlApi'
import moment from 'moment'
import {Breadcrumbs, Grid, Link} from '@mui/material'
import Layout from '../../components/Layout'
import {CustomPager} from '../../components/Helper/Pagination/CustomPager'
// import {fetchReviewAsMeta} from '../../src/lmsApi'
// import {SEOCourseHead} from '../../components/Helper/PageHead/SEOHead'
import {useRouter} from 'next/router'


const NewsPage = (props: any) => {
    const {mainPosts, totalPages, currentPage
        // , reviewMeta
    } = props



    // const router = useRouter()
    // const lang = router.query.lang ?? consts.defaultLang
    return (
        <Layout>
            {/*<SEOCourseHead*/}
            {/*    reviewMeta={reviewMeta}*/}
            {/*    name={`Yongecareer`}*/}
            {/*    description={`Yongecareer`}*/}
            {/*    url={`${baseURL}/${lang}/news/${currentPage}`}*/}
            {/*/>*/}
            <section className={`section componentContainer middle`}>
                <Breadcrumbs>
                    <Link href={`/`}>{''}</Link>
                    <Link href={`/news/1`}>新闻资讯</Link>
                </Breadcrumbs>
                <div className="container">
                    <Grid container className="row rowTitle">
                        <Grid item xs={12} className="col-12">
                            <h1 className="underlineHeaderCenter heading text-center mt-16">{`新闻资讯`}</h1>
                        </Grid>
                    </Grid>

                    <Grid container direction="row" className="row">
                        {/* Left Articles */}
                        <Grid item xs={12} md={8}>
                            <Grid container justifyContent="center" className={`pb  mb-8`}>
                                {mainPosts.map((da: any, idx: number) => (
                                    <Grid item key={idx} xs={12}>
                                        <NewsArticleCard data={da} />
                                    </Grid>
                                ))}

                                <CustomPager link={`/news`} currentPage={currentPage} totalPages={totalPages} />
                            </Grid>
                        </Grid>

                        {/* Right Related Content */}

                        <Grid item xs={12} md={4}>
                            <Grid container></Grid>
                        </Grid>
                    </Grid>
                </div>
            </section>
        </Layout>
    )
}

export const getStaticPaths = async () => {
    let next = ''
    let totalPages = 1
    let hasNext = true

    while (hasNext) {
        const wpData = await getAllNewsArticleSlugs(POSTS_PER_PAGE, next)
        if (wpData?.pageInfo?.hasNextPage) {
            totalPages++
            next = wpData?.pageInfo?.endCursor
        } else {
            hasNext = false
        }
    }

    const paths = [] as any
    for (let i = 1; i <= totalPages; i++) {
        paths.push({params: {page: i.toString()}})
    }

    return {
        paths,
        fallback: false
    }
}

export const getStaticProps = async (props: any) => {
    let page = 1
    try {
        page = parseInt(props.params.page)
    } catch (e) {
        // Not an int
        console.log('News page param not a valid int!')
    }

    const filterPosts = (posts: any) => {
        const res = posts.map((item: any, index: number) => {
            const image = getPostImage(item.node)
            const title = item.node.title
            const date = moment(item.node.dateGmt).format('L')
            // const description = item.node?.seo?.metaDesc
            const link = `/news/article/${item.node.slug}`
            return {image, title,
                // description,
                link, date}
        })
        return res || []
    }
    // const reviewMeta = await fetchReviewAsMeta('yc-offer-service')
    const filterMorePosts = (posts: any, isVid: any) => {
        try {
            const res = posts[0]?.node?.posts?.nodes.map((post: any, index: number) => {
                return {
                    title: post.title,
                    date: moment(post.dateGmt).format('L'),
                    link: isVid ? getPostVideoUrl(post) : `/testimonials/student-cases/case/${post.slug}`,
                    newTab: isVid
                }
            })
            return res || []
        } catch (e) {
            console.log('Cannot filter more posts in news', e)
            return []
        }
    }

    // Get main articles
    // still need to loop through to calculate how many pages and obtain content
    let next = ''
    let hasNext = true
    let articleListData = [] as any
    let totalPages = 1
    while (hasNext) {
        let wpData
        if (totalPages === page) {
            // The current page to load, fetch all the details
            wpData = await getAllNewsArticles(POSTS_PER_PAGE, next)
            // Dump current data
            articleListData = wpData.edges

        } else {
            // Not the page we want, fetch minimal details
            wpData = await getAllNewsArticleSlugs(POSTS_PER_PAGE, next)
        }
        if (wpData?.pageInfo?.hasNextPage) {
            next = wpData?.pageInfo?.endCursor
            totalPages++
        } else {
            hasNext = false
        }
    }

    let mainPosts = []
    if (articleListData) {
        mainPosts = filterPosts(articleListData)
    }

    // Get side video posts
    const videoData = await getVideoPostsForNews()
    let videoPosts = []
    if (videoData.edges) {
        videoPosts = filterMorePosts(videoData.edges, true)
    }

    // Get side article posts
    const articleData = await getArticlePostsForNews()
    let articlePosts = []
    if (articleData.edges) {
        articlePosts = filterMorePosts(articleData.edges, false)
    }

    return {
        props: {mainPosts, videoPosts, articlePosts, totalPages, currentPage: page
            // , reviewMeta
        },
        revalidate: 1
    }
}

export default withLocaleStaticPage(NewsPage)
