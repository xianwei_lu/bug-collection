import React from 'react'
import withLocaleStaticPage from '../../../hocs/withLocaleStatic'
import {
    getAllNewsArticles,
    getPostAuthor,
    getPostAuthorAvatar,
    getPostBySlug,
    getPostDescription,
    getPostImage
} from '../../../src/graphqlApi'
import consts, {baseURL, CNX_, POSTS_PER_PAGE, SEOMapHTML} from '../../../consts'
import {useRouter} from 'next/router'
import Layout from '../../../components/Layout'
// import {fetchReviewAsMeta} from '../../../src/lmsApi'
// import {SEOCourseHead} from '../../../components/Helper/PageHead/SEOHead'
import TestimonialHeader from "../../../components/testimonials/GraphQLTestimonialView/TestimonialHeader";
import TestimonialLayout from "../../../components/testimonials/GraphQLTestimonialView/TestimonialLayout";


const NewsArticle = (props: any) => {
    const {post, slugsAndTitles
        // , reviewMeta
    } = props
    const router = useRouter()
    // const lang = router.query.lang ?? consts.defaultLang
    return (
        <Layout>
            {/* For review meta */}
            {/*<SEOCourseHead*/}
            {/*    reviewMeta={reviewMeta}*/}
            {/*    name={`Yongecareer`}*/}
            {/*    description={`Yongecareer`}*/}
            {/*    url={`${baseURL}/${lang}`}*/}
            {/*/>*/}
            <section className="section">
                {router.isFallback ? (
                    <div className="container">
                        <div className="row rowTitle">
                            <div className="col-12">
                                <h1 className="underlineHeaderCenter heading">Please wait...</h1>
                                <p>
                                    The page might be loading. If it does not refresh within a few seconds then the page
                                    might have been lost. Please try a different link.
                                </p>
                            </div>
                        </div>
                    </div>
                ) : (
                    <>
                        <TestimonialHeader title={post.title} description={post.title} image={post.image} />
                        <div className="container">
                            <TestimonialLayout post={post} morePosts={[]} slugs={slugsAndTitles} />
                        </div>
                    </>
                )}
            </section>
        </Layout>
    )
}

export const getStaticPaths = async () => {
    let next = ''
    let hasNext = true
    let articleListData = [] as any

    while (hasNext) {
        const wpData = await getAllNewsArticles(POSTS_PER_PAGE, next)
        articleListData = [...articleListData, ...wpData?.edges]
        if (wpData?.pageInfo?.hasNextPage) {
            next = wpData.pageInfo.endCursor
        } else {
            hasNext = false
        }
    }

    let slugs = []
    if (articleListData.length > 0) {
        slugs = articleListData.map((node: any) => {
            return {title: node.node.title, slug: node.node.slug}
        })
    }

    slugs = slugs.slice(0, Math.min(slugs.length - 1, 5))

    const paths = slugs.map((item: any, index: number) => ({params: {slug: item.slug, title: item.title}}))

    return {
        paths,
        fallback: 'blocking'
    }
}

interface ImageMetaProps {
    mediaItemUrl: string
    altText: string
    mediaDetails: any
}

interface ImageDetailsProps {
    url: string
    altText: string
    width: string
    height: string
}

// interface SeoDataProps {
//     [key: string]: any
//     title: string
//     description: string
//     openGraph: {
//         site_name: string
//         title: string
//         description: string
//         images: ImageDetailsProps[]
//     }
// }

// interface SEOMapHTMLProps {
//     [key: string]: any
//     title: string
//     metaDesc: string
// }

export const getStaticProps = async (props: any) => {
    // const reviewMeta = await fetchReviewAsMeta('yc-offer-service')
    const filterPost = (post: any) => {
        if (post) {
            const postDetails = post.content ?? ''
            const contentSplitter = postDetails.indexOf('<hr />')
            const content = contentSplitter !== -1 ? postDetails.slice(contentSplitter + 6) : postDetails

            const title = post.title
            const description = getPostDescription(post)
            const image = getPostImage(post)
            const author = getPostAuthor(post)
            const authorAvatar = getPostAuthorAvatar(post)
            const dateTime = post.dateGmt

            // return {}
            return {content, title, description, image, author, authorAvatar, dateTime}
        } else {
            return {}
        }
    }

    let notFound = true
    let postInfo = {}
    // const seoData: SeoDataProps = {
    //     title: '',
    //     description: '',
    //     openGraph: {site_name: '', title: '', description: '', images: []}
    // }

    const currentPost = await getPostBySlug(props.params.slug)
    const allSlugsAndTitles = await getStaticPaths()

    if (!!currentPost && currentPost.id) {
        postInfo = filterPost(currentPost)
        if (currentPost) notFound = false
    }

    // try {
    //     // Get metadata
    //
    //     for (const [key, value] of Object.entries(currentPost.seo)) {
    //         const html: SEOMapHTMLProps = SEOMapHTML
    //         if (key in html) {
    //             // export const SEOMapHTML = {
    //             //     title: 'title',
    //             //     metaDesc: 'description'
    //             // }
    //
    //             seoData[html[key]] = value
    //         } else if (key === 'site_name') {
    //             if (typeof value === 'string') {
    //                 seoData.openGraph.site_name = value
    //             }
    //         } else if (key === 'title') {
    //             if (typeof value === 'string') {
    //                 seoData.openGraph.title = value
    //             }
    //         } else if (key === 'description') {
    //             if (typeof value === 'string') {
    //                 seoData.openGraph.description = value
    //             }
    //         } else if (key === 'opengraphImage') {
    //             try {
    //                 const imageValue = value as ImageMetaProps
    //                 const imageDetails: ImageDetailsProps = {url: '', altText: '', height: '', width: ''}
    //                 imageDetails.url = imageValue.mediaItemUrl
    //                 imageDetails.altText = imageValue.altText
    //                 imageDetails.height = imageValue.mediaDetails.height
    //
    //                 imageDetails.width = imageValue.mediaDetails.width
    //                 seoData.openGraph.images = [imageDetails]
    //             } catch (e) {
    //                 console.log(`Error obtaining image ${CNX_} for case: ${currentPost.id}`)
    //             }
    //         }
    //     }
    // } catch (e) {
    //     console.log('Error getting post information:\n', e)
    // }

    return {
        props: {post: postInfo,
            // meta: seoData,
            slugsAndTitles: allSlugsAndTitles
            // , reviewMeta
        },
        revalidate: 1,
        notFound
    }
}

export default withLocaleStaticPage(NewsArticle)
