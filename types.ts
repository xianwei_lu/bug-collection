export enum SYSLANG {
    en = 'en',
    zh = 'zh',
    tw = 'tw'
}
