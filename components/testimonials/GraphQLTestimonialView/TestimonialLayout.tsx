import React from 'react'
import TestimonialReadMore from './TestimonialReadMore'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import {TestimonialContent} from "./TestimonialContent";


interface TestimonialLayoutProps {
    post: {
        author: string
        authorAvatar: string
        content: string
        dateTime: string
        description: string
        image: string
        title: string
    }
    morePosts: []
    slugs: {
        paths: Array<{params: {slug: string; title: string}}>
        fallback: string
    }
}

const TestimonialLayout: React.FC<TestimonialLayoutProps> = ({post, morePosts, slugs}) => {
    const currentTitle = post.title
    const allSlugs = slugs.paths.map((item: any) => item.params.slug)
    const allTitles = slugs.paths.map((item: any) => item.params.title)

    const id = allTitles.findIndex((item: any) => item === currentTitle)
    let prevId = id - 1
    if (prevId < 0) {
        prevId = prevId + allSlugs.length
    }

    let nextId = id + 1

    if (nextId >= allSlugs.length) {
        nextId = nextId - allSlugs.length
    }

    return (
        <div className={`marginTop500 marginBottom100`}>
            <TestimonialContent
                content={post.content}
                author={post.author}
                authorAvatar={post.authorAvatar}
                dateTime={post.dateTime}
            />
            {morePosts.length > 0 && <TestimonialReadMore posts={morePosts} />}

            <Box className={`nextArticleButton`}>
                <Button size="large" href={`/news/article/${allSlugs[prevId]}`}>
                    {`${allTitles[prevId]}`}
                </Button>
                <Button href={`/news/article/${allSlugs[nextId]}`}>{`${allTitles[nextId]}`}</Button>
            </Box>
        </div>
    )
}

export default TestimonialLayout
