import React from 'react'
import styles from './TestimonialHeader.module.scss'
import Image from 'next/image'
import {Breadcrumbs, Typography} from '@mui/material'
import HomeIcon from '@mui/icons-material/Home'
import Link from 'next/link'
import {useRouter} from 'next/router'

interface TestimonialHeaderProps {
    title: string
    description: string
    image: string
}

const TestimonialHeader: React.FC<TestimonialHeaderProps> = ({title, description, image}) => {
    const router = useRouter()
    return (
        <div>
            <div className={styles.bannerWrapper}>
                <div className={styles.bannerImage}>
                    <Image src={image} alt="student image" layout="fill" objectFit="cover" />
                </div>

                <div className={styles.bannerContent}>
                    <Typography variant="h1">{description}</Typography>
                    <Breadcrumbs aria-label="breadcrumb" className={styles.breadcrumb} style={{zIndex: '99'}}>
                        <Link href={`/`}>
                            <HomeIcon fontSize="small" style={{paddingBottom: '2px'}} />
                        </Link>
                        <Link href={`/news/1`}>{'新闻资讯'}</Link>
                        <Link href={`${router?.asPath}`}>{title}</Link>
                    </Breadcrumbs>
                </div>
            </div>
        </div>
    )
}

export default TestimonialHeader
