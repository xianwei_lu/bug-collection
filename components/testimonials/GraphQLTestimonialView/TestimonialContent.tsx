import React from 'react'
import styles from './TestimonialContent.module.scss'
import moment from 'moment'
import {makeStyles} from '@mui/styles'
import Image from 'next/legacy/image'
import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'

const useStyles = makeStyles(() => ({
    content: {
        '& .alignright': {
            position: 'relative',
            left: '100%',
            transform: 'translate(-100%, 0)'
        }
    }
}))

interface TestimonialContentProps {
    content: string
    author: string
    authorAvatar: string
    dateTime: string
}

export const TestimonialContent: React.FC<TestimonialContentProps> = ({content, author, authorAvatar, dateTime}) => {
    const classes = useStyles()
    return (
        <Box>
            <Grid container>
                <Grid item xs={0.5} lg={1}></Grid>
                <Grid item xs={11} lg={10}>
                    <div>
                        <div className={styles.detailWrapper}>
                            <div className={styles.detailContainer}>
                                <div className={styles.author}>{author}</div>
                                <div className={styles.dateTime}>{moment(dateTime).utcOffset(-10).format('LLL')}</div>
                            </div>
                            <div className={styles.avatar}>
                                <Image src={authorAvatar} alt="" width={96} height={96} />
                            </div>
                        </div>
                        <div
                            className={`${styles.content} ${classes.content}`}
                            dangerouslySetInnerHTML={{__html: content || ''}}
                        />
                    </div>
                </Grid>
                <Grid item xs={0.5} lg={1}></Grid>
            </Grid>
        </Box>
    )
}

// export default TestimonialContent
