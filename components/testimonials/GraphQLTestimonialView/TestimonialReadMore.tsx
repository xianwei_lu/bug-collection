import React from 'react'
import Link from 'next/link'
import {getPostDescription, getPostImage, getPostTitle} from '../../../src/graphqlApi'
import {makeStyles} from '@mui/styles'
import {Button, Typography} from '@mui/material'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import CardList, {CardType} from "../../Helper/VideoCard/CardList";

const useStyles = makeStyles(theme => ({
    caseContainer: {
        marginTop: '100px'
    },
    button: {
        marginTop: '50px',
        '& button': {
            // margin: theme.spacing(2),
            whiteSpace: 'nowrap',
            '& span': {
                // color: theme.palette.primary.light,
                fontWeight: 'bold',
                '&:hover': {
                    // color: theme.palette.primary.main
                }
            }
        }
    }
}))

const TestimonialReadMore = (props: any) => {
    const classes = useStyles()

    const filterPost = (props: any) => {
        try {
            if (!!props.posts && props.posts.node) {
                const node = props.posts.node
                const title = getPostTitle(node)
                const description = getPostDescription(node)
                const src = getPostImage(node)
                const url = `/testimonials/student-cases/case/${node.slug}`
                const type = CardType.IMAGE

                return {title, description, src, url, type}
            } else {
                return {}
            }
        } catch (e) {
            console.log('Error getting more posts:\n', e)
            return {}
        }
    }

    const morePosts = props.posts.map((p: any) => filterPost(p))

    return (
        <div>
            {morePosts.length > 0 && (
                <div className={classes.caseContainer}>
                    <Typography variant="h4">More Cases</Typography>
                    <CardList data={morePosts} />
                </div>
            )}
            <div className={classes.button}>
                <Link href={`/testimonials/student-cases`}>
                    <Button
                        variant="outlined"
                        color="primary"
                        startIcon={<ArrowBackIcon color="primary" />}
                        size="large"
                    >
                        See More Cases
                    </Button>
                </Link>
            </div>
        </div>
    )
}

export default TestimonialReadMore
