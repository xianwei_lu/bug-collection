import React from 'react'
import clsx from 'clsx'
import {makeStyles} from '@mui/styles'
import FooterYC from 'components/blocks/footer/FooterBB'
import NavbarYC from '../src/components/blocks/navbar/NavbarBB'
import PageProgress from '../src/components/common/PageProgress'
import NextLink from '../src/components/reuseable/links/NextLink'

interface Props {
    title?: string
    content?: string
    keywords?: string
    footerBehind?: boolean
    children: any
}

const useStyles = makeStyles(() => ({
    // For pages that doesn't want the footer to block everything
    footerBehind: {
        '& footer': {
            zIndex: -1,
            position: 'absolute'
        }
    },
    floatingButtonLink: {
        textDecoration: 'none'
    },
    floatingButton: {
        padding: '0.5rem 0.5rem calc(0.5rem + env(safe-area-inset-bottom))',
        height: '40px',
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        maxWidth: '100%!important',
        textDecoration: 'none!important',
        color: 'white',
        background: 'rgba(44, 66, 125, .95)',
        boxShadow: '0px -2px 10px 0px rgba(0, 0, 0, 1)',
        opacity: 1,
        transition: 'all 0.3s ease-in-out',
        '&:hover': {
            opacity: 1,
            background: 'rgba(44, 66, 125, 1)'
        }
    }
}))

// footerBehind forces the footer to go behind on pages that doesn't want it in front of everything
const Layout: React.FC<Props> = ({title, content, keywords, footerBehind = false, children}) => {
    const classes = useStyles()

    return (
        <React.Fragment>
            <div>
                {
                    <div className="layoutMain">
                        <PageProgress />

                        {/* ========== header section ========== */}
                        <header className="wrapper bg-soft-primary">
                            <NavbarYC
                                // language
                                logo={'yc-h-bluetext-whiteback'}
                                logoSticky="yc-h-bluetext-whiteback"
                                navClassName="navbar navbar-expand-lg center-nav navbar-light navbar-bg-light"
                                button={
                                    <NextLink
                                        title="Contact"
                                        href="/contact-us"
                                        className="btn btn-sm btn-primary rounded"
                                    />
                                }
                            />
                        </header>

                        <>{children}</>
                        <div className={clsx(footerBehind ? classes.footerBehind : '')}>
                            <FooterYC />
                        </div>
                    </div>
                }
            </div>
        </React.Fragment>
    )
}

export default Layout
