import React from 'react'
import Link from 'next/link'
import AnimatedButtonCard from './AnimatedButtonCard'
import {Backdrop, CardMedia, Grid} from '@mui/material'
import styles from './CardList.module.scss'
import {getYoutubeLinkFromString} from '../../../src/helperFunctions'

export enum CardType {
    VIDEO = 'Video',
    IMAGE = 'Image'
}

export interface AnimatedButtonCardProp {
    type: CardType
    src: string
    url: string
    rippleText: string[]
    title: string
    description: string
}

interface CardListProp {
    data: AnimatedButtonCardProp[]
}

const CardList = ({data}: CardListProp) => {
    const [isVideoOpen, setIsVideoOpen] = React.useState(false)
    const [videoUrl, setVideoUrl] = React.useState('')

    const handleClose = () => {
        setIsVideoOpen(false)
        setVideoUrl('')
    }

    const loadVideo = (url: any) => {
        setIsVideoOpen(true)
        if (!url || url.length === 0) return ''
        let videoSrc = ''
        if (url.includes('youtu')) videoSrc = getYoutubeLinkFromString(url)
        else videoSrc = url
        setVideoUrl(videoSrc)
    }

    return (
        <div>
            <Backdrop className={styles.backdrop} open={isVideoOpen} onClick={handleClose}>
                <div className={styles.frameContainer}>
                    <div className={styles.frameWrapper}>
                        {videoUrl.length > 0 && (
                            <CardMedia
                                className={styles.iframeContent}
                                component="iframe"
                                title="Testimony"
                                src={videoUrl}
                            />
                        )}
                    </div>
                </div>
            </Backdrop>

            <Grid container alignItems="center" direction="row" className={styles.gridContainer}>
                {data.map((c, i) => {
                    const {type, src, url, rippleText, title, description} = c
                    return (
                        <Grid item key={i} xs={12} sm={12} md={6} className={styles.gridItem}>
                            {type === CardType.VIDEO ? (
                                <AnimatedButtonCard
                                    type={type}
                                    src={src}
                                    url={url}
                                    rippleText={rippleText}
                                    title={title}
                                    description={description}
                                    onSetVideo={loadVideo}
                                />
                            ) : (
                                <Link href={url}>
                                    <AnimatedButtonCard
                                        type={type}
                                        src={src}
                                        url={url}
                                        rippleText={rippleText}
                                        title={title}
                                        description={description}
                                        onSetVideo={loadVideo}
                                    />
                                </Link>
                            )}
                        </Grid>
                    )
                })}
            </Grid>
        </div>
    )
}

export default CardList
