import React from 'react'
import styles from './AnimatedButtonCard.module.scss'
import Card from '@mui/material/Card'
import Image from 'next/legacy/image'
import PlayCircleFilledWhiteOutlinedIcon from '@mui/icons-material/PlayCircleFilledWhiteOutlined'
import {CardType} from './CardList'
import clsx from 'clsx'
import {makeStyles} from '@mui/styles'

const AnimatedButtonCard = (card: any) => {
    const {type, src, url, rippleText, title, description, onSetVideo} = card
    const rippleTextList = rippleText || []
    const totalRips = rippleTextList.length
    const totalTime = 5 + totalRips * 0.25

    const useStyles = makeStyles(() => ({
        rippleAnimation: {
            animation: `${styles.ripple} linear infinite ${totalTime}s`
        }
    }))

    const classes = useStyles()

    return (
        <div className={styles.cardOuter}>
            <div className={styles.cardWrapper}>
                <Card className={styles.card}>
                    <div className={styles.imageWrapper}>
                        <Image src={src} alt="card image" layout="fill" objectFit="cover" />
                    </div>

                    <div
                        className={clsx(styles.textWrapper, {
                            [styles.textWrapperIsImage]: type === CardType.IMAGE
                        })}
                    >
                        <div className={styles.textContainer}>
                            {rippleTextList.map((text: any, i: number) => (
                                <div
                                    key={i}
                                    className={classes.rippleAnimation}
                                    style={{animationDelay: `${i * (totalTime / totalRips)}s`}}
                                >
                                    <p>{text}</p>
                                </div>
                            ))}
                        </div>

                        {type === CardType.VIDEO && (
                            <div className={styles.playButton}>
                                <PlayCircleFilledWhiteOutlinedIcon
                                    onClick={() => {
                                        onSetVideo(url)
                                    }}
                                />
                            </div>
                        )}
                    </div>

                    <div
                        className={clsx(styles.hoverBanner, {
                            [styles.hoverBannerIsImage]: type === CardType.IMAGE
                        })}
                    >
                        <a
                            className={styles.firstLine}
                            onClick={() => {
                                onSetVideo(url)
                            }}
                        >
                            {title}
                        </a>
                        <div className={styles.secondLine}>{description}</div>
                    </div>
                </Card>
            </div>
        </div>
    )
}

export default AnimatedButtonCard
