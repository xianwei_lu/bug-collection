import {useRouter} from 'next/router'
import Pagination from '@mui/material/Pagination'
import * as React from 'react'
import {useState} from 'react'
import {PaginationItem} from '@mui/material'
import {Link, MemoryRouter, Route, Routes} from 'react-router-dom'
import {Box} from '@mui/system'

interface ContentProps {
    currentPage: number
    totalPages: number
    link: string
}

const Content: React.FC<ContentProps> = ({currentPage, totalPages, link}) => {
    const router = useRouter()
    const [isRedirecting, setIsRedirecting] = useState(false)
    return (
        <div>
            {isRedirecting ? (
                <Pagination
                    page={currentPage}
                    count={totalPages}
                    disabled={true}
                    renderItem={item => (
                        <Box>
                            <PaginationItem style={{margin: '0 5px'}} {...item} />
                        </Box>
                    )}
                />
            ) : (
                <Pagination
                    variant="outlined"
                    color="primary"
                    page={currentPage}
                    count={totalPages}
                    disabled={false}
                    renderItem={item => (
                        <Box
                            onClick={() => {
                                if (!!item?.page && item?.page > 0 && item.page <= totalPages) {
                                    setIsRedirecting(true)
                                    router.push(`${link}/${item.page === 1 ? `1` : `${item.page}`}`).then(() => {
                                        setIsRedirecting(false)
                                    })
                                }
                            }}
                        >
                            <PaginationItem
                                style={{margin: '0 5px '}}
                                component={Link}
                                to={`${link}/${item.page === 1 ? `1` : `${item.page}`}`}
                                {...item}
                            />
                        </Box>
                    )}
                />
            )}
        </div>
    )
}

interface ContentProps {
    currentPage: number
    totalPages: number
    link: string
}

export const CustomPager: React.FC<ContentProps> = ({currentPage, totalPages, link}) => {
    return (
        <MemoryRouter initialIndex={0}>
            <Routes>
                <Route path="*" element={<Content currentPage={currentPage} totalPages={totalPages} link={link} />} />
            </Routes>
        </MemoryRouter>
    )
}
