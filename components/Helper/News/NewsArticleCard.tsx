import React from 'react'
import styles from './NewsArticleCard.module.scss'
import Image from 'next/legacy/image'
import Link from 'next/link'

interface NewsArticleCardProps {
    data: {
        date: string
        // description: string
        image: string
        link: string
        title: string
    }
}

const NewsArticleCard: React.FC<NewsArticleCardProps> = ({data}) => {
    // const cleanedDescription = data?.description.slice(0, 80)
    console.log(`data.link`, data.link)

    return (
        <div className={styles.newsCardRoot}>
            <div className={styles.newsCardImageWrapper}>
                <Link href={data.link}>
                    <Image src={data.image} alt="card image" layout="fill" objectFit="cover" />
                </Link>
            </div>
            <div className={styles.newsCardDetails}>
                <div className={styles.content}>
                    <Link href={data.link}>
                        <h2>{data.title}</h2>
                    </Link>
                    <p className={styles.date}>{data.date}</p>
                    {/*<p className={styles.excerpt}>{cleanedDescription}</p>*/}
                </div>
            </div>
        </div>
    )
}

export default NewsArticleCard
