import React from 'react'
import {isLocale, type Locale} from '../translations/types'
import {LocaleProvider} from '../context/LocaleContext'
import {getDisplayName} from 'next/dist/shared/lib/utils'
import {type NextPage} from 'next'

interface LangProps {
    locale?: Locale
}

export async function getStaticProps(ctx: any) {
    const pageProps = {}
    if (typeof ctx.query.lang !== 'string' || !isLocale(ctx.query.lang)) {
        return {...pageProps, locale: undefined}
    }
    return {...pageProps, locale: ctx.query.lang}
}

const withLocaleStaticPage = (WrappedPage: NextPage<any>) => {
    const WithLocale: NextPage<any, LangProps> = ({locale, ...pageProps}) => {
        if (!locale) {
            locale = 'zh'
        }
        return (
            <LocaleProvider lang={locale}>
                <WrappedPage {...pageProps} />
            </LocaleProvider>
        )
    }
    WithLocale.displayName = `withLang(${getDisplayName(WrappedPage)})`
    return WithLocale
}

export default withLocaleStaticPage
