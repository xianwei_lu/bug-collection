/** @type {import('next').NextConfig} */
const nextConfig = {
  swcMinify: true,
  // reactStrictMode: false
  images: {
    domains: ['winbigbass.com', 'lh3.googleusercontent.com', 'secure.gravatar.com', '0.gravatar.com'],
    disableStaticImages: true
  },
};

module.exports = nextConfig;
