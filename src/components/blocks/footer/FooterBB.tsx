import React, {useState} from 'react'
// -------- custom component -------- //
import NextLink from 'components/reuseable/links/NextLink'
import parse from 'html-react-parser'
// -------- data -------- //
import {footerNav} from 'data/footerDataBB'
import {OFWEB_ADDRESS, OFWEB_DATE} from '../../../../consts'
import moment from 'moment/moment'
import Image from 'next/image'

const FooterBB: React.FC = () => {
    const [showPhone, setShowPhone] = useState(false)
    const [showEmail, setShowEmail] = useState(false)

    return (
        <footer className="bg-navy text-inverse footerYC">
            <div className="container pt-10 pt-md-10 pb-13 pb-md-15">
                <hr className="mt-11 mb-12" />

                <div className="row gy-6 gy-lg-0">
                    <div className="col-md-4 col-lg-3">
                        <div className="widget">
                            <Image width={190} height={65} className="mb-4" src="/img/yc-h-whiteext-trans.svg" alt="" />

                            <p className="mb-4">
                                {OFWEB_ADDRESS.name}
                                <br />
                                {OFWEB_ADDRESS.nameZh} <br />© {OFWEB_DATE.foundedDate} - {moment().format('YYYY')}
                                <br className="d-none d-lg-block" />
                                All Rights Reserved.
                            </p>

                            {/* <SocialLinks className="nav social social-white" /> */}
                        </div>
                    </div>

                    <div className="col-md-4 col-lg-3">
                        <div className="widget">
                            <h4 className="widget-title text-white mb-3">{parse(`Get in Touch<br/>联系我们`)}</h4>
                            <address className="pe-xl-15 pe-xxl-17">{OFWEB_ADDRESS.address}</address>
                            <div
                                className="mb-n6 cursor"
                                onClick={() => {
                                    setShowEmail(true)
                                }}
                            >
                                {showEmail
                                    ? OFWEB_ADDRESS.email
                                    : OFWEB_ADDRESS.email.slice(0, 1) +
                                    OFWEB_ADDRESS.email.slice(1, 3).replace(/[0-9a-zA-Z]/g, '*') +
                                    OFWEB_ADDRESS.email.slice(3)}
                            </div>
                            <br />
                            <div
                                onClick={() => {
                                    setShowPhone(true)
                                }}
                                className="cursor"
                            >
                                {showPhone
                                    ? OFWEB_ADDRESS.phone
                                    : OFWEB_ADDRESS.phone.slice(0, 3) +
                                    OFWEB_ADDRESS.phone.slice(3, 11).replace(/.(?=...)/g, '*') +
                                    OFWEB_ADDRESS.phone.slice(10)}
                            </div>
                        </div>
                    </div>

                    <div className="col-md-4 col-lg-3">
                        <div className="widget">
                            <h4 className="widget-title text-white mb-3">{parse(`Quick Service<br/>快速服务`)}</h4>
                            <ul className="list-unstyled  mb-0">
                                {footerNav.map(({title, url}) => (
                                    <li key={title}>
                                        <NextLink title={title} href={url} />
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>

                    <div className="col-md-12 col-lg-3">
                        <div className="widget">
                            <h4 className="widget-title text-white mb-3">联系客服</h4>
                            <Image width={190} height={190} className="mb-4" src="/img/qrcode-yc.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default FooterBB
