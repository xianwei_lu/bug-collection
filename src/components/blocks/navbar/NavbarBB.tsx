import React, {Fragment, type ReactElement, useRef, useState} from 'react'
import clsx from 'clsx'
// -------- custom hook -------- //
import useSticky from 'hooks/useSticky'
// -------- custom component -------- //
import NextLink from 'components/reuseable/links/NextLink'
import ListItemLink from 'components/reuseable/links/ListItemLink'
import DropdownToggleLink from 'components/reuseable/links/DropdownToggleLink'
// -------- partial header component -------- //
import Info from './partials/Info'
import Search from './partials/Search'
import Social from './partials/Social'
import Signin from './partials/Signin'
import Signup from './partials/Signup'
import Language from './partials/Language'
import MiniCart from './partials/MiniCart'
// -------- data -------- //
import {navBar} from 'data/navigation'
import Image from 'next/image'
import {OFWEB_ADDRESS} from '../../../../consts'
import {Divider} from '@mui/material'

// ===================================================================
interface NavbarProps {
    logo: string
    logoSticky: string
    info?: boolean
    cart?: boolean
    fancy?: boolean
    logoAlt?: string
    search?: boolean
    social?: boolean
    language?: boolean
    stickyBox?: boolean
    navClassName?: string
    button?: ReactElement
    navOtherClass?: string
}
// ===================================================================

const NavbarBB: React.FC<NavbarProps> = props => {
    const {
        logo: logoNormal,
        logoSticky,
        navClassName,
        info,
        search,
        social,
        language,
        button,
        cart,
        fancy,
        navOtherClass,
        stickyBox
    } = props

    const sticky = useSticky(330)
    const navbarRef = useRef<HTMLElement | null>(null)

    const [sideBarShow, setSideBarShow] = useState(false)

    const [menuId, setMenuId] = useState(-1)

    // dynamically render the logo
    const logo = sticky ? logoNormal : logoSticky
    // dynamically added navbar classname
    const fixedClassName = 'navbar navbar-expand-lg center-nav transparent navbar-light navbar-clone fixed'

    const [showPhone, setShowPhone] = useState(false)
    const [showEmail, setShowEmail] = useState(false)
    // box-shadow: 0px 5px 30px 0px rgb(0 0 0 / 10%);
    // transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
    // render inner nav item links

    // the function of trigger open sidebar

    // all main header contents
    const headerContent = (
        <Fragment>
            <div className="navbar-brand w-100">
                <NextLink
                    href="/"
                    title={<Image width={120} height={(55 / 90) * 120} alt="logo" src={`/img/bugssite-logo.svg`} />}
                />
            </div>

            <div
                id="offcanvas-nav"
                data-bs-scroll="true"
                className={clsx('navbar-collapse offcanvas offcanvas-nav offcanvas-start', sideBarShow && 'show')}
            >
                <div className="offcanvas-header d-lg-none">
                    <NextLink
                        href="/"
                        title={
                            <Image
                                width={120}
                                height={(55 / 90) * 120}
                                alt="logo"
                                src={`/img/bugssite-logo.svg`}
                            />
                        }
                    />

                    <button
                        type="button"
                        aria-label="Close"
                        data-bs-dismiss="offcanvas"
                        className="btn-close btn-close-white"
                        onClick={() => {
                            setSideBarShow(false)
                        }}
                    />
                </div>

                <div className="offcanvas-body ms-lg-auto d-flex flex-column h-100">
                    <ul className="navbar-nav">
                        {/*  =====================  nav item map  ===================== */}

                        {navBar.map((item, index) => (
                            <li
                                className="nav-item dropdown me-2 ms-2"
                                key={index}
                                onClick={() => {
                                    index === menuId ? setMenuId(-1) : setMenuId(index)
                                }}
                            >
                                <DropdownToggleLink title={item.title} className="nav-link dropdown-toggle" />

                                <ul className={clsx('dropdown-menu', menuId === index && 'show')}>
                                    <div style={{marginLeft: '10px'}}>
                                        {index !== 4
                                            ? item.dropdownItem.map(({id, url, title}) => {
                                                return (
                                                    <ListItemLink
                                                        key={id}
                                                        href={url}
                                                        title={title}
                                                        linkClassName="dropdown-item"
                                                    />
                                                )
                                            })
                                            : item.dropdownItem.map(({id, url, title}) => {
                                                return (
                                                    <div key={id}>
                                                        <ListItemLink
                                                            key={id}
                                                            href={url}
                                                            title={title}
                                                            linkClassName="dropdown-item"
                                                        />
                                                        {id === 2 && (
                                                            <Divider
                                                                light
                                                                variant="middle"
                                                                className={`mt-2 mb-2 borderColor`}
                                                            />
                                                        )}
                                                    </div>
                                                )
                                            })}
                                    </div>
                                </ul>
                            </li>
                        ))}
                    </ul>

                    {/* ============= show contact-us info in the small device sidebar ============= */}
                    <div className="offcanvas-footer d-lg-none">
                        <div>
                            <div
                                onClick={() => {
                                    setShowEmail(true)
                                }}
                            >
                                <NextLink
                                    title={
                                        showEmail
                                            ? OFWEB_ADDRESS.email
                                            : OFWEB_ADDRESS.email.slice(0, 1) +
                                            OFWEB_ADDRESS.email.slice(1, 3).replace(/[0-9a-zA-Z]/g, '*') +
                                            OFWEB_ADDRESS.email.slice(3)
                                    }
                                    className="link-inverse"
                                    href="#"
                                />
                            </div>

                            <div
                                onClick={() => {
                                    setShowPhone(true)
                                }}
                            >
                                <NextLink
                                    href="#"
                                    className="link-inverse"
                                    title={
                                        showPhone
                                            ? OFWEB_ADDRESS.phone
                                            : OFWEB_ADDRESS.phone.slice(0, 3) +
                                            OFWEB_ADDRESS.phone.slice(3, 11).replace(/.(?=...)/g, '*') +
                                            OFWEB_ADDRESS.phone.slice(10)
                                    }
                                />
                            </div>

                            <br />
                        </div>
                    </div>
                </div>
            </div>

            {/* ============= right side header content ============= */}
            <div className={navOtherClass}>
                <ul className="navbar-nav flex-row align-items-center ms-auto">
                    {/* ============= language dropdwown ============= */}
                    {language && <Language />}

                    {/* ============= info button ============= */}
                    {info && (
                        <li className="nav-item">
                            <a className="nav-link" data-bs-toggle="offcanvas" data-bs-target="#offcanvas-info">
                                <i className="uil uil-info-circle" />
                            </a>
                        </li>
                    )}

                    {/* ============= search icon button ============= */}
                    {search && (
                        <li className="nav-item">
                            <a className="nav-link" data-bs-toggle="offcanvas" data-bs-target="#offcanvas-search">
                                <i className="uil uil-search" />
                            </a>
                        </li>
                    )}

                    {/* ============= contact-us button ============= */}
                    {button && <li className="nav-item d-none d-md-block">{button}</li>}

                    {/* ============= shopping cart button ============= */}
                    {cart && (
                        <li className="nav-item">
                            <a
                                data-bs-toggle="offcanvas"
                                data-bs-target="#offcanvas-cart"
                                className="nav-link position-relative d-flex flex-row align-items-center"
                            >
                                <i className="uil uil-shopping-cart" />
                                <span className="badge badge-cart bg-primary">3</span>
                            </a>
                        </li>
                    )}

                    {/* ============= social icons link ============= */}
                    {social && <Social />}

                    {/* ============= humburger button for small device ============= */}
                    <li className="nav-item d-lg-none">
                        <button
                            data-bs-toggle="offcanvas"
                            data-bs-target="#offcanvas-nav"
                            className="hamburger offcanvas-nav-btn"
                            onClick={() => {
                                setSideBarShow(!sideBarShow)
                            }}
                        >
                            <span />
                        </button>
                    </li>
                </ul>
            </div>
        </Fragment>
    )

    return (
        <Fragment>
            {stickyBox && <div style={{paddingTop: sticky ? navbarRef.current?.clientHeight : 0}} />}

            <nav ref={navbarRef} className={sticky ? fixedClassName : navClassName}>
                {fancy ? (
                    <div className="container">
                        <div className="navbar-collapse-wrapper bg-white d-flex flex-row flex-nowrap w-100 justify-content-between align-items-center">
                            {headerContent}
                        </div>
                    </div>
                ) : (
                    <div className="container flex-lg-row flex-nowrap align-items-center">{headerContent}</div>
                )}
            </nav>

            {/* ============= signin modal ============= */}
            <Signin />

            {/* ============= signup modal ============= */}
            <Signup />

            {/* ============= info sidebar ============= */}
            {info && <Info />}

            {/* ============= show search box ============= */}
            {search && <Search />}

            {/* ============= cart sidebar ============= */}
            {cart && <MiniCart />}
        </Fragment>
    )
}

// set deafult Props
NavbarBB.defaultProps = {
    cart: false,
    info: false,
    social: false,
    search: false,
    language: false,
    stickyBox: true,
    navOtherClass: 'navbar-other w-100 d-flex ms-auto',
    navClassName: 'navbar navbar-expand-lg center-nav transparent navbar-light'
}

export default NavbarBB
