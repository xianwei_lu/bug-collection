import {NewsCategory} from "./consts";

export const API_URL = 'http://winbigbass.com/graphql'
// export const API_URL = 'https://wptest.winbigcareer.com/graphql'

const fetchAPI = async (query, { variables } = {}) => {
    const headers = { 'Content-Type': 'application/json' }

    const res = await fetch(API_URL, {
        method: 'POST',
        headers,
        body: JSON.stringify({ query, variables }),
    })

    const json = await res.json()
    if (json.errors) {
        console.error(json.errors)
        throw new Error('Failed to fetch API')
    }
    return json.data
}

export const getAllNewsArticles = async (first, after) => {
    try {
        const data = await fetchAPI(
            `
            query AllArticles($first: Int, $after: String) {
              posts(first: $first, after: $after, where: {categoryName: "${NewsCategory.BugCollection}"}) {
                edges {
                  node {
                    featuredImage {
                      node {
                        sourceUrl
                      }
                    }
                    slug
                    id
                    dateGmt
                    title
                    author {
                      node {
                      avatar {
                          url
                        }
                        name
                      }
                    }
                  }
                }
                pageInfo {
                  hasNextPage
                  endCursor
                }
              }
            }
            `,
            {
                variables: {first, after}
            }
        )
        return data?.posts
    } catch (e) {
        console.log('Error', e)
        return {}
    }
}

export const getArticlePostsForNews = async () => {
    try {
        const data = await fetchAPI(
            `
            query FetchVideoPosts {
              postFormats(where: {nameLike: "Image"}) {
                edges {
                  node {
                    id
                    posts(first: 3, where: {categoryName: "${NewsCategory.BugCollection}"}) {
                      nodes {
                        slug
                        title
                        dateGmt
                      }
                    }
                  }
                }
              }
            }
            `
        )
        return data?.postFormats
    } catch (e) {
        console.log('Error', e)
        return {}
    }
}
export const getAllNewsArticleSlugs = async (first, after) => {
    try {
        const data = await fetchAPI(
            `
            query AllTestimonialImageSlugs($first: Int, $after: String) {
              posts(first: $first, after: $after, where: {categoryName: "${NewsCategory.BugCollection}"}) {
                edges {
                  node {
                    slug
                    postFormats {
                      nodes {
                        name
                      }
                    }
                    id
                  }
                }
                pageInfo {
                  hasNextPage
                  endCursor
                }
              }
            }
            `,
            {
                variables: {first, after}
            }
        )
        return data?.posts
    } catch (e) {
        console.log('Error', e)
        return {}
    }
}

export const DEFAULT_IMG = '/img/testimonial/broken_image.jpg'
export const getPostImage = node => {
    try {
        const image = node.featuredImage?.node?.sourceUrl || DEFAULT_IMG
        return encodeURI(image)
    } catch (e) {
        console.log('Error', e)
        // default image
        return DEFAULT_IMG
    }
}

export const getArticleForHome = async () => {
    try {
        const data = await fetchAPI(
            `
            query AllArticles {
              posts(first: 10, where: {categoryName: "${NewsCategory.BugCollection}", orderby: {field: DATE, order: DESC}}) {
                edges {
                  node {
                    featuredImage {
                      node {
                        sourceUrl
                      }
                    }
                    slug
                    id
                    dateGmt
                    title
                  }
                }
              }
            }
            `
        )
        return data?.posts
    } catch (e) {
        console.log('Error', e)
        return {}
    }
}

export const getPostVideoUrl = node => {
    const content = node.content ?? ''
    let url = ''
    // Get Video link if it's a video
    const vidLine = /<iframe.*?src="(.*?)"/.exec(content)
    url = vidLine ? vidLine[1] : ''

    // Was not a YouTube embed, see if using inline video tag
    if (url === '') {
        const vidLineInLine = /<p>case_video:.*?(.*?)<\/p>/i.exec(content)
        url = vidLineInLine ? vidLineInLine[1] : ''
    }
    return url
}

export const getVideoPostsForNews = async () => {
    try {
        const data = await fetchAPI(
            `
            query FetchVideoPosts {
              postFormats(where: {nameLike: "Video"}) {
                edges {
                  node {
                    id
                    posts(first: 3, where: {categoryName: "${NewsCategory.BugCollection}"}) {
                      nodes {
                        slug
                        title
                        dateGmt
                        content
                      }
                    }
                  }
                }
              }
            }
            `
        )
        return data?.postFormats
    } catch (e) {
        console.log('Error in getVideoPostsForCarousel posts ok-->', e)
        return {}
    }
}

export const getPostAuthor = node => {
    try {
        return node.author?.node?.name || 'Nobody'
    } catch (e) {
        console.log('Error', e)
        return ''
    }
}

export const getPostAuthorAvatar = node => {
    try {
        return node.author?.node?.avatar?.url || DEFAULT_IMG
    } catch (e) {
        console.log('Error', e)
        return DEFAULT_IMG
    }
}

export const getPostBySlug = async slug => {
    slug = encodeURIComponent(slug.toLowerCase()).toLowerCase()

    try {
        const data = await fetchAPI(
            `
            query FindSlug($slug: ID!) {
              post(id: $slug, idType: SLUG) {
                featuredImage {
                  node {
                    sourceUrl
                  }
                }
                author {
                  node {
                  avatar {
                      url
                    }
                    name
                  }
                }
                postFormats {
                  nodes {
                    name
                  }
                }
                id
                slug
                content
                dateGmt
                title
              }
            }
            `,
            {
                variables: {slug}
            }
        )
        return data?.post
    } catch (e) {
        console.log('Error', e)
        return {}
    }
}

export const getPostDescription = node => {
    const content = node.content ?? ''
    const descLine = /<p>case_description:.*?(.*?)<\/p>/i.exec(content)
    return descLine ? descLine[1] : ''
}


export const getPostTitle = node => {
    const content = node.content ?? ''
    const titleLine = /<p>case_name:.*?(.*?)<\/p>/i.exec(content)
    return titleLine ? titleLine[1] : ''
}