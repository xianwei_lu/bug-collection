export const getYoutubeLinkFromString = (url: any) => {
    const regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/
    let match = url.match(regExp)
    match = match.map((m: any, i: any) => {
        if (!!m && m.includes('</a>')) {
            return m.replace('</a>', '')
        }
        return m
    })
    const videoID = (!!match && match.find((e: any, i: any) => e.length === 11)) || ''
    return videoID.length === 11 ? `https://www.youtube.com/embed/${videoID}` : videoID
}