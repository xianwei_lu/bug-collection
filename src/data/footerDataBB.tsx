export const footerNav = [
    {title: '优创加拿大移民', url: '/immigration'},
    {title: '优创工作', url: '/job'},
    {title: '优创留学', url: '/study'},
    {title: '优创签证', url: '/visa'}
]